/*
MIT License

Copyright (c) 2021 JakeV

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package me.jake.spigotresourceapi;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * Created by Jake on 1/5/2021.
 * <insert description here>
 */
public class ResourceInfo {
    private int id;
    private String title;
    private String tag;
    private String currentVersion;

    private int authorId;
    private String authorUsername;

    private float price;
    private String currency;

    private int downloads;
    private int updates;
    private int reviews;
    private float rating;

    private ResourceInfo(){

    }
    static ResourceInfo fromData(DataInputStream stream) throws IOException {
        ResourceInfo resourceInfo = new ResourceInfo();
        resourceInfo.deserialize(stream);
        return resourceInfo;
    }

    @Override
    public String toString() {
        return "ResourceInfo{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", tag='" + tag + '\'' +
                ", currentVersion='" + currentVersion + '\'' +
                ", authorId=" + authorId +
                ", authorUsername='" + authorUsername + '\'' +
                ", price=" + price +
                ", currency='" + currency + '\'' +
                ", downloads=" + downloads +
                ", updates=" + updates +
                ", reviews=" + reviews +
                ", rating=" + rating +
                '}';
    }

    void deserialize(DataInputStream in) throws IOException {
        id = in.readInt();
        title = in.readUTF();
        tag = in.readUTF();
        currentVersion = in.readUTF();
        authorId = in.readInt();
        authorUsername = in.readUTF();
        price = in.readFloat();
        currency = in.readUTF();
        downloads = in.readInt();
        updates = in.readInt();
        reviews = in.readInt();
        rating = in.readFloat();
    }
    //GETTERS

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getTag() {
        return tag;
    }

    public String getCurrentVersion() {
        return currentVersion;
    }

    public int getAuthorId() {
        return authorId;
    }

    public String getAuthorUsername() {
        return authorUsername;
    }

    public float getPrice() {
        return price;
    }

    public String getCurrency() {
        return currency;
    }

    public int getDownloads() {
        return downloads;
    }

    public int getUpdates() {
        return updates;
    }

    public int getReviews() {
        return reviews;
    }

    public float getRating() {
        return rating;
    }

    //
}
